# skillcheckbot
This repo is my attempt to create skillcheck bot for dead by daylight.
All code was originally developed by me in Feb 2022.
In Feb 2023, I rewrote the code for readability and can share it with you.
That's all, serious.

## Files
[**bot.py**](bot.py)
> main script, responsible for frames, time, action button (hopefully soon)

[**internals.py**](internals.py)
> currently showing this information about shown skill check: great area position (angular + cartesian), arrow position (angular + cartesian), residual angle, arrow angular velocity, frame time

[**xn.flv**](xn.flv)
> 1280x720@60fps skillcheck sample. recorded on my friend's high-performance computer

[**xr.flv**](xr.flv)
> 1280x720@23.59fps skilcheck sample. recorded on my own computer via geforce now, with lags


## About impl
To work, it need to know skillcheck circle area (depends on game resolution), circle size, circle center coords.How it works step-by-step:
1. Calculate cirle mask on init (pixles that affected by circle)
2. Calculate count of red, white and gray pixels every frame.
3. If there is many gray pixels it mean that skillcheck is shown.
4. If skillcheck is just shown, calculate white zone angle
5. If red arrow moved, calculate it's angular velocity and append it to velocitys array
6. Calculate median velocity and approx touch time

## Usage video
![](usage.mp4)
