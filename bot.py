import time
from internals import FrameAnalyzer
import cv2

if __name__ == "__main__":
    
    # 720p settings
    area = (596, 316, 684, 404)
    width = 88
    height = 88


    fa = FrameAnalyzer(width, height, (44, 44), 44)
    fs = cv2.VideoCapture('xn.flv')

    fnum = 0
    frame_time = 0
    while 7:
        fnum += 1

        alive, frame = fs.read()
        if fnum < 100:
            continue

        if not alive:        
            exit("Video stream closed.")
    
        f = frame[area[1]:area[3], area[0]:area[2]] #crop to area
        f = f[:,:,::-1] #bgr->rgb (for cv sources)

        frame_time += 1 / 60  #60 fps rec

        fa.push(f, frame_time)
        
        print(F"It was frame #{fnum}")
        if fnum > 100:
            input()
        #input()