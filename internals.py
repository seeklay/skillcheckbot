import math
import numpy
import cv2
import statistics

class FrameAnalyzer:

    def __init__(self, width, height, center, radius):

        self.width = width
        self.height = height
        self.center = center
        self.radius = radius
        self.shown = False

        cover = numpy.ndarray(shape=(height, width, 3), dtype=numpy.uint8)
        cover.fill(27)
        cv2.circle(cover, center, radius, (255, 0, 255), 1)                
        cover = cover.tobytes()
        self.cover = [cover[i:i+3] for i in range(0, len(cover), 3)]
    
    def mask(self, frame_pix):

        red_pix, white_pix, gray_pix = [], [], 0
        overlay = b""

        for i, p in enumerate(self.cover):

            # if we on the mask
            if p == b"\xff\x00\xff":

                # if it red pixel
                if frame_pix[i][0] >= 120 and frame_pix[i][1] <= 50 and frame_pix[i][2] <= 50:
                    red_pix.append(divmod(i, self.width))

                # if it red pixel
                elif frame_pix[i][0] >=84  and frame_pix[i][1] <= 20 and frame_pix[i][2] <= 20:
                    red_pix.append(divmod(i, self.width))
                
                # if white red pixel
                elif frame_pix[i][0] >= 240 and frame_pix[i][1] >= 240 and frame_pix[i][2] >= 240:
                    white_pix.append(divmod(i, self.width))                                      

                # if it gray pixel
                elif frame_pix[i][0] >= 100 and frame_pix[i][1] >= 100 and frame_pix[i][2] >= 100:
                    if frame_pix[i][0] <= 135 and frame_pix[i][1] <= 135 and frame_pix[i][2] <= 135:
                        gray_pix += 1
                # create overlay
                overlay += frame_pix[i]
            else:
                overlay += p
        return red_pix, white_pix, gray_pix, overlay

    def sort(self, points, clockwise=True):

        current = points[0]

        for point in points:
            if clockwise:
                if point[1] > self.center[1]:
                    if point[0] > current[0]:    
                        current = point
                else:
                    if point[0] < current[0]:
                        current = point
            else:
                if point[1] < self.center[1]:
                    if point[0] > current[0]:                      
                        current = point                             
                else:
                    if point[0] < current[0]:
                        current = point   
        return current

    def CalcSqPixFAngCanv(self, axay):
        x = axay[1]-self.center[1]
        y = self.center[0]-axay[0]
        return x, y

    def angle(self, v1, v2):
        sm = v1[0]*v2[0]+v1[1]*v2[1]
        m1 = math.sqrt(v1[0]**2+v1[1]**2)
        m2 = math.sqrt(v2[0]**2+v2[1]**2)
        cs = sm/(m1*m2)
        dg = math.degrees(math.acos(cs))        
        if v2[0] < 0:
            ang = (180-dg)+180
        else:
             ang = dg
        return ang

    def push(self, frame, frame_time):

        frame_raw = frame.tobytes()
        frame_pix = [frame_raw[i:i+3] for i in range(0, len(frame_raw), 3)]
        red, white, gray, overlay = self.mask(frame_pix)
        
        olay = numpy.ndarray(shape=(self.height, self.width, 3), buffer=overlay, dtype=numpy.uint8)                

        text = numpy.ndarray(shape=(self.height*3, self.width*3, 3), dtype=numpy.uint8)
        text.fill(0)
        cv2.line(text, (0, 0), (self.width*3, 0), (127,127,127), 1)
        cv2.putText(text, f'Red: {len(red)}', (10, 40), cv2.FONT_HERSHEY_PLAIN, 1.2, (200, 200, 200), 1, 2)
        cv2.putText(text, f'White: {len(white)}', (10, 60), cv2.FONT_HERSHEY_PLAIN, 1.2, (200, 200, 200), 1, 2)
        cv2.putText(text, f'Gray: {gray}', (10, 80), cv2.FONT_HERSHEY_PLAIN, 1.2, (200, 200, 200), 1, 2)        

        cv2.putText(text, f'Frame time: {frame_time}', (10, 244), cv2.FONT_HERSHEY_PLAIN, 1.2, (200, 200, 200), 1, 2)

        if gray > 20:      
            if not self.shown:
                self.shown = True      
                self.white_zone = self.sort(white, False)
                self.white_vec = self.CalcSqPixFAngCanv(self.white_zone)
                self.white_angle = round(self.angle((0, 1), self.white_vec), 2)
                self.show_time = frame_time
                arrow = self.sort(red)
                arrow_vec = self.CalcSqPixFAngCanv(arrow)
                self.arrow_angle = round(self.angle((0, 1), arrow_vec), 2)
                self.frame_time = 1
                self.speeds = []
        else:
            if self.shown:
                self.shown = False
            cv2.putText(text, f'Circle show: False', (10, 20), cv2.FONT_HERSHEY_PLAIN, 1.2, (200, 200, 200), 1, 2)
        
        if self.shown:
            arrow = self.sort(red)
            arrow_vec = self.CalcSqPixFAngCanv(arrow)
            arrow_angle = round(self.angle((0, 1), arrow_vec), 2)

            arrow_ang_vel = round(arrow_angle - self.arrow_angle, 2) / (frame_time - self.frame_time)                        
            self.arrow_angle = arrow_angle
            self.frame_time = frame_time

            remaining_angle = abs(arrow_angle - self.white_angle)
            if arrow_ang_vel > 0:
                self.speeds.append(arrow_ang_vel)
                ms = statistics.median(self.speeds)
                tt = (remaining_angle / ms) + frame_time
                print(F"Mean speed: {ms}, if it true touch time will be {tt}")

            cv2.putText(text, f'Circle show: True', (10, 20), cv2.FONT_HERSHEY_PLAIN, 1.2, (200, 200, 200), 1, 2)            
            cv2.putText(text, f'Pos: {arrow} Ang: {arrow_angle}', (10, 120), cv2.FONT_HERSHEY_PLAIN, 1.2, (250, 127, 127), 1, 2)
            cv2.putText(text, f'Pos: {self.white_zone} Ang: {self.white_angle}', (10, 140), cv2.FONT_HERSHEY_PLAIN, 1.2, (250, 250, 250), 1, 2)
            cv2.putText(text, f'Remaining angle: {remaining_angle}', (10, 160), cv2.FONT_HERSHEY_PLAIN, 1.2, (224, 86, 253), 1, 2)
            cv2.putText(text, f'Arrow avel: {arrow_ang_vel}', (10, 180), cv2.FONT_HERSHEY_PLAIN, 1.2, (255, 190, 118), 1, 2)
            
            cv2.line(olay, (self.center[0], self.center[1]), (self.white_zone[1], self.white_zone[0]), (127,127,127), 1)
            cv2.line(olay, (self.center[0], self.center[1]), (arrow[1], arrow[0]), (255,127,127), 1)

        scene = cv2.resize(olay, (self.width*3, self.height*3), interpolation=cv2.INTER_NEAREST)
        scene = numpy.concatenate((scene, text))
        scene = scene[:,:,::-1] #rgb->bgr (for cv outputs)
        
        cv2.imshow('f', scene)
        cv2.waitKey(1)